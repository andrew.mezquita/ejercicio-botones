package com.example.ejerciciobotones.ui.theme

import androidx.compose.ui.graphics.Color

val PkBlack = Color(0xFF000000)
val PkWhite = Color(0xFFFFFFFF)